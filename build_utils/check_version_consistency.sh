#!/bin/bash

declare -A VERSIONS

VERSIONS['GIT_TAG']="$(\
  git describe --tags --abbrev=0 |\
  sed -e 's/^v//'\
)"

VERSIONS['SRC_SPEC']="$(\
  grep -e "VERSION\s*=" src/spec/create_extension_spec.py |\
  sed -e 's/VERSION\s*=//' |\
  tr -d \'\" |\
  sed -e 's/^\s*//; s/\s*$//'\
)"


VERSIONS['FILE']="$(cat src/pynwb/*/VERSION)"

VERSIONS['YAML']="$(\
  grep version spec/*namespace.y*ml |\
  sed -e 's/version\s*:\s*//' |\
  tr -d \'\" |\
  sed -e 's/^\s*//; s/\s*$//'\
)"

for _src_ in "${!VERSIONS[@]}"; do 
  echo -e "VERSION from ${_src_}: \t ${VERSIONS[${_src_}]}"
done

UNIQUE_VERSION="$(\
  echo -e "${VERSIONS[@]}" |\
  tr ' ' '\n' |\
  sort -u
)"

NUM_UNQ_VER="$(wc -l <<< "$UNIQUE_VERSION")"

if [ "$NUM_UNQ_VER" -eq 1 ] ; then 
  echo -e "Consistent VERSION: \t $UNIQUE_VERSION"
  export OFFICIAL_VERSION="$UNIQUE_VERSION"
  echo "$OFFICIAL_VERSION"
else
  echo "Inconsistent or no versions found across files"
  echo "Found $NUM_UNQ_VER versions:"
  echo "$UNIQUE_VERSION"
  exit 1
fi 

