import datetime
from string import ascii_letters, digits

import hypothesis as hpt
import numpy as np
from hdmf.common import DynamicTable, VectorData
from hypothesis import strategies as st
from ndx_acquisition_module import AcquisitionModule
from pynwb.testing import AcquisitionH5IOMixin, TestCase, remove_test_file

from pynwb import NWBHDF5IO, NWBFile, TimeSeries

const_examples = [
    dict(module_id=None, timeseries_ids=[None], table_ids=[None]),
    dict(module_id="just_series", timeseries_ids=["odor", "sound", None], table_ids=[]),
    dict(module_id="just_table", timeseries_ids=[], table_ids=["odor", "sound", None]),
    dict(
        module_id=10,
        timeseries_ids=[2, None, "abc", "one2Three"],
        table_ids=[None, "test", 43],
    ),
]

_allowable_text = st.text(ascii_letters + digits + "_-,.:", min_size=1, max_size=10)
_allowable_id = st.one_of(st.floats(), st.integers(), _allowable_text)
_allowable_list = st.lists(elements=_allowable_id, min_size=1, max_size=10, unique=True)
_allowable_gen_idset = st.fixed_dictionaries(
    dict(
        module_id=_allowable_id,
        timeseries_ids=_allowable_list,
        table_ids=_allowable_list,
    )
)
_allowable_idset_list = st.lists(
    elements=_allowable_gen_idset,
    min_size=3,
    max_size=10,
    unique_by=lambda d: str(d["module_id"]),
).filter(
    lambda D: len(_tsr_flat := np.concatenate([d["timeseries_ids"] for d in D]))
    == len(set(_tsr_flat))
    and len(_tbl_flat := np.concatenate([d["table_ids"] for d in D]))
    == len(set(_tbl_flat))
)


def set_up_nwbfile():
    nwbfile = NWBFile(
        session_description="session_description",
        identifier="identifier",
        session_start_time=datetime.datetime.now(datetime.timezone.utc),
    )

    return nwbfile


def _prepare_timeseries_params(_id=None):
    _suffix = ""
    if _id is not None:
        _suffix = f"_{_id}"
    sz = np.random.randint(20, 1000)
    params = dict(
        name=f"raw_series{_suffix}",
        description="raw time series",
        data=np.random.randint(4, size=sz),
        timestamps=(np.arange(sz).astype("float") + 2) / 30,
        unit="-",
    )
    return {f"timeseries{_suffix}": params}


def _prepare_dynamictable_params(_id=None):
    _suffix = ""
    if _id is not None:
        _suffix = f"_{_id}"
    metadata = dict(
        name=f"lookup_table{_suffix}",
        description="metadata lookup table for raw_series",
    )
    columns = [
        dict(
            name=f"lookup_id{_suffix}", description="ID to look up", data=[0, 1, 2, 3]
        ),
        dict(
            name=f"lookup_name{_suffix}",
            description="name of each ID",
            data=[
                f"water{_suffix}",
                f"earth{_suffix}",
                f"fire{_suffix}",
                f"air{_suffix}",
            ],
        ),
    ]

    return {f"table_metadata{_suffix}": metadata, f"table_columns{_suffix}": columns}


def _prepare_module_params(_id=None):
    _suffix = ""
    if _id is not None:
        _suffix = f"_{_id}"

    metadata = dict(
        name=f"raw_module{_suffix}",
        description="raw module with a series and a dynamic table",
    )

    return metadata


def _flatten_listofdicts(L):
    D = dict()
    for x in L:
        D.update(**x)
    return D


def create_params(module_id=None, timeseries_ids=[None], table_ids=[None]):
    timeseries_params = [_prepare_timeseries_params(s_id) for s_id in timeseries_ids]
    table_params = [_prepare_dynamictable_params(t_id) for t_id in table_ids]
    return dict(
        module=_prepare_module_params(module_id),
        **_flatten_listofdicts(timeseries_params),
        **_flatten_listofdicts(table_params),
    )


def create_acqmod(ids_set):
    params = create_params(**ids_set)
    mod = AcquisitionModule(**params["module"])
    for obj_name, obj_params in params.items():
        if obj_name == "module" or "columns" in obj_name:
            continue
        if "timeseries" in obj_name:
            mod.add(TimeSeries(**obj_params))
            continue
        if "table_metadata" in obj_name:
            _col_key = obj_name.replace("metadata", "columns")
            mod.add(
                DynamicTable(
                    **obj_params,
                    columns=[
                        VectorData(**_col_params) for _col_params in params[_col_key]
                    ],
                )
            )
    return mod, params


def assert_mod(testcase_obj, mod, params):
    for obj_name, obj_params in params.items():
        if obj_name == "module":
            objs = [mod]
        elif "columns" not in obj_name:
            assert (
                _obj := mod.data_interfaces.get(obj_params["name"], None)
            ) is not None
            objs = [_obj]
        else:
            _tbl_key = obj_name.replace("columns", "metadata")
            assert (
                _tbl := mod.data_interfaces.get(params[_tbl_key]["name"], None)
            ) is not None
            objs = [getattr(_tbl, _col_params["name"]) for _col_params in obj_params]

        if type(obj_params) is not list:
            obj_params = [obj_params]

        for _obj, _params in zip(objs, obj_params):
            for _param_name, _param_val in _params.items():
                _obj_prm_val = getattr(_obj, _param_name)
                compare_fn = testcase_obj.assertEqual

                if isinstance(_param_val, np.ndarray):
                    if issubclass(_param_val.dtype.type, np.floating):
                        compare_fn = np.testing.assert_array_equal
                    else:
                        compare_fn = np.testing.assert_equal
                hpt.note(
                    f"AT `{obj_name}.{_param_name}` with \n\t - value = {_obj_prm_val} \n\t - expect = {_param_val}"
                )
                compare_fn(_obj_prm_val, _param_val)


class TestAcquisitionModuleConstructor(TestCase):
    """Test basic functionality of AcquisitionModule without read/write"""

    def setUp(self):
        """Set up an NWB file."""
        self.nwbfile = set_up_nwbfile()

    @hpt.given(ids_set=_allowable_gen_idset)
    @hpt.example(ids_set=const_examples[0])
    @hpt.example(ids_set=const_examples[1])
    @hpt.example(ids_set=const_examples[2])
    @hpt.example(ids_set=const_examples[3])
    def test_constructor(self, ids_set):
        """Test the constructor"""
        # Create
        mod, params = create_acqmod(ids_set)

        # Test
        assert_mod(self, mod, params)
        # for obj_name, obj_params in params.items():


class TestAcquisitionModuleRoundtrip(TestCase):
    """Simple roundtrip test for AcquisitionModule."""

    def setUp(self):
        self.nwbfile = set_up_nwbfile()
        self.path = "test.nwb"

    def tearDown(self):
        remove_test_file(self.path)

    @hpt.settings(
        max_examples=50,
        deadline=10000,  # 10 sec
        suppress_health_check=(
            hpt.HealthCheck.filter_too_much,
            hpt.HealthCheck.too_slow,
        ),
        verbosity=hpt.Verbosity.normal,
    )
    @hpt.given(idset_list=_allowable_idset_list)
    @hpt.example(idset_list=const_examples)
    def test_roundtrip(self, idset_list):
        self.setUp()  # need to manually add here to clear nwbfile variable

        hpt.note(f"PARAMS{idset_list}\n{self.nwbfile.acquisition}")
        hpt.event(f"LENGTH of list = {len(idset_list)}")

        mod_params = []
        for eg in idset_list:
            mod, params = create_acqmod(eg)
            mod_params.append((mod, params))
            self.nwbfile.add_acquisition(mod)

        with NWBHDF5IO(self.path, mode="w") as io:
            io.write(self.nwbfile)

        with NWBHDF5IO(self.path, mode="r", load_namespaces=True) as io:
            read_nwbfile = io.read()
            for mod, params in mod_params:
                self.assertContainerEqual(
                    mod, read_nwbfile.acquisition[params["module"]["name"]]
                )

        self.tearDown()  # manually add here to delete file just in case


class TestAcquisitionModuleRoudndtripPyNWB(AcquisitionH5IOMixin, TestCase):
    """Complex, more complete roundtrip test for AcquisitionModule using pynwb.testing infrastructure."""

    def setUpContainer(self):
        """Return the test AcquisitionModule to read/write"""
        mod, params = create_acqmod(const_examples[-1])
        self._acqmod_name = params["module"]["name"]
        return mod

    def addContainer(self, nwbfile):
        """Add the test AcquisitionModule to the given NWBFile."""
        nwbfile.add_acquisition(self.container)

    def getContainer(self, nwbfile):
        """Get the AcquisitionModule object from the given NWBFile."""
        return nwbfile.get_acquisition(self._acqmod_name)


class TestReadmeEXample(TestCase):
    """Run the example that is show in the README"""

    def setUp(self):
        self.path = "test.nwb"

    def tearDown(self):
        remove_test_file(self.path)

    def test_readme_script(self):
        from datetime import datetime

        import numpy as np
        from dateutil import tz
        from hdmf.common import DynamicTable, VectorData
        from ndx_acquisition_module import AcquisitionModule

        from pynwb import NWBHDF5IO, NWBFile, TimeSeries

        # Create an example NWBFile
        nwbfile = NWBFile(
            session_description="test session description",
            identifier="unique_identifier",
            session_start_time=datetime(2012, 2, 21, tzinfo=tz.gettz("US/Pacific")),
        )

        # Create time series
        ts = TimeSeries(
            name="choice_series",
            description="raw choice series",
            data=np.random.randint(4, size=100),
            timestamps=(np.arange(100).astype("float") + 2) / 30,
            unit="-",
        )

        # Create dynamic table
        tbl = DynamicTable(
            name="lookup_table",
            description="lookup table for `choice_series`",
            columns=[
                VectorData(
                    name="lookup_id", description="ID to look up", data=[0, 1, 2, 3]
                ),
                VectorData(
                    name="lookup_name",
                    description="name of ID",
                    data=["water", "earth", "fire", "air"],
                ),
            ],
        )

        # Create AcquisitionModule to store these objects
        mod = AcquisitionModule(name="raw_mod", description="raw acq module")

        # Add data objects to created AcquisitionModule
        mod.add(ts)  # add time series
        mod.add(tbl)  # add dynamic table

        # Add AcquisitionModule to nwbfile.acquisition
        nwbfile.add_acquisition(mod)

        # Write the file to disk
        filename = "test.nwb"
        with NWBHDF5IO(path=filename, mode="w") as io:
            io.write(nwbfile)

        # Read file and check
        with NWBHDF5IO(filename, mode="r", load_namespaces=True) as io:
            read_nwbfile = io.read()
            self.assertContainerEqual(mod, read_nwbfile.acquisition["raw_mod"])
