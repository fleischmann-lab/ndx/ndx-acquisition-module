# -*- coding: utf-8 -*-
import os.path

from pynwb.spec import NWBAttributeSpec, NWBGroupSpec, NWBNamespaceBuilder, export_spec


def main():
    # define version and write to file
    VERSION = "0.1.2"
    version_path = os.path.abspath(
        os.path.join(
            os.path.dirname(__file__),
            "..",
            "pynwb",
            "ndx_acquisition_module",
            "VERSION",
        )
    )
    with open(version_path, "w") as f:
        f.write(VERSION)

    # define metadata
    ns_builder = NWBNamespaceBuilder(
        doc="Extension to add acquisition module",
        name="ndx-acquisition-module",
        version=VERSION,
        author=["Tuan Pham"],
        contact=["tuanhpham@brown.edu"],
    )

    ns_builder.include_type("NWBDataInterface", namespace="core")
    ns_builder.include_type("DynamicTable", namespace="hdmf-common")

    acq_mod = NWBGroupSpec(
        neurodata_type_def="AcquisitionModule",
        neurodata_type_inc="NWBDataInterface",
        doc="Module/container to add acquisition raw data into",
        quantity="*",
        attributes=[
            NWBAttributeSpec(
                name="description",
                doc="Description of this acquisition module.",
                dtype="text",
            ),
        ],
        groups=[
            NWBGroupSpec(
                neurodata_type_inc="NWBDataInterface",
                doc="Data objects stored in this acquisition collection.",
                quantity="*",
            ),
            NWBGroupSpec(
                neurodata_type_inc="DynamicTable",
                doc="Tables stored in this acquisition collection.",
                quantity="*",
            ),
        ],
    )

    new_data_types = [acq_mod]

    # export the spec to yaml files in the spec folder
    output_dir = os.path.abspath(
        os.path.join(os.path.dirname(__file__), "..", "..", "spec")
    )
    export_spec(ns_builder, new_data_types, output_dir)
    print(
        "Spec files generated. Please make sure to rerun `pip install .` to load the changes."
    )


if __name__ == "__main__":
    # usage: python create_extension_spec.py
    main()
